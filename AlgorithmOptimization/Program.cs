﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmOptimization
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        static int singleNumber(int[] A, int n)
        {
            var dictionary = new Dictionary<int, int>();

            for(int i = 0; i < n; i++)
            {
                if (dictionary.ContainsKey(A[i]))
                    dictionary[A[i]]++;
                else
                    dictionary.Add(A[i], 1);
            }

            var result = dictionary.SingleOrDefault(x => x.Value == 1);
            if (!result.Equals(default(KeyValuePair<int, int>)))
                return result.Key;

            throw new ArgumentException("Wrong data.");
        }
    }
}
